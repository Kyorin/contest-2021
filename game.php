<?php
    include("stat.php");

    if(!isset($_GET['choice'])){    
        $_SESSION["stat"] = null;
        $_SESSION["historyComputer"] = null;
    }

    if(isset($_SESSION["stat"])){
        $stat = unserialize($_SESSION["stat"]);
    }
    else {
        $stat = new Stat();
        $_SESSION['startTime'] = date("Y-m-d H:i:s");
    }

    // Création du tableau avec les valeurs pour le tirage de l'ordinateur et du joueur
    $posibility = array('Pierre', 'Feuille', 'Ciseaux');

    function getDrawComputer($values, $stat, $choice){
        $tour = $stat->getNbParty() % 5;

        if(isset($_SESSION["historyComputer"])){
            $historyComputer = $_SESSION["historyComputer"];
        }
        else {
            $historyComputer = [0,0,0];
        }

        $result;
        switch ($tour) {
            case 0: //aléatoire
                $result = array_rand($values);
                $_SESSION['tour1_computer'] = $result;
                break;
            case 1: //tour 2
                if($choice == 0){
                    $result = 1;
                }
                elseif($choice == 1){
                    $result = 2;
                }
                else {
                    $result = 0;
                }
                break;
            case 2: //tour 3
                $value = $_SESSION['tour1_computer'];
                $_SESSION['tour1_computer'] = null;
                $result = $value;
                break;
            case 3: //tour 4
                $result = array_search(min($historyComputer), $historyComputer);
                $_SESSION['tour4_player'] = $choice;
                break;
            case 4: //tour 5
                $result = $_SESSION['tour4_player'];
                
                break;
        }
        $historyComputer[$result] = microtime(true);

        $_SESSION["historyComputer"] = $historyComputer;
        return $result;
    }

    $choice = null;
    $drawComputer = null;
    $result = "";

if(isset($_GET['choice'])){

    $choice = $_GET['choice'];

    $choiceText = $posibility[$choice];
    $drawComputer = getDrawComputer($posibility, $stat, $choice);
    $drawComputerText = $posibility[$drawComputer];

    if ($choice == $drawComputer)
    {
        $result = 'Vous êtes à égalité';
        $stat->addEquality();
    }
    elseif($choiceText == 'Pierre' && $drawComputerText == 'Ciseaux' ||
        $choiceText == 'Feuille' && $drawComputerText == 'Pierre' ||
        $choiceText == 'Ciseaux' && $drawComputerText == 'Feuille')
    {
        $result = 'Vous avez gagné';
        $stat->addVictory();            
    }
    else {
        $result = 'Vous avez été battu';
        $stat->addParty();
    }


    $_SESSION["stat"] = serialize($stat);
}
?>