<?php
    class Stat {
        protected $nbParty = 0;
        protected $nbVictory = 0;
        protected $nbEgality = 0;

        public function getNbParty(){
            return $this->nbParty;
        }

        public function getVictory(){
            return $this->nbVictory;
        }

        public function getEgality(){
            return $this->nbEgality;
        }

        public function addVictory(){
            $this->nbVictory += 1;
            $this->nbParty += 1;
        }
        public function addEquality(){
            $this->nbEgality += 1;
            $this->nbParty += 1;
        }
        public function addParty(){
            $this->nbParty += 1;
        }
    }
?>