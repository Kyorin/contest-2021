<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shifumi</title>
    <link rel="stylesheet" href="assets/css/style.css">
</head>
    <?php include("game.php"); ?>

<body>

    <?php 
        $servername = 'localhost';
        $username = 'root';
        $password = '';
        
        try{
            $conn = new PDO("mysql:host=$servername;dbname=pierre_feuille_ciseau", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
          }
    ?>

    <header>
        <h1>Shifumi</h1>
    </header> 

    <?php if($choice != null){ ?>
    <div class="container info">
        <span><?php echo $result; ?></span>
    </div>
    <?php } ?>

    <div class="main">
        <div class="container player">
            <button type="button" <?php echo "class='btn".($choice == '0' ? " playerSelection" : "")."'" ?> href="javascript:void(0)" onclick="location.href='index.php?choice=0'">
                <img src="./assets/img/rock.png" alt="pierre"/>
            </button>

            <button type="button" <?php echo "class='btn".($choice == '1' ? " playerSelection" : "")."'" ?> href="javascript:void(0)" onclick="location.href='index.php?choice=1'">
                <img src="./assets/img/paper.png" alt="feuille"/>
            </button>

            <button type="button" <?php echo "class='btn".($choice == '2' ? " playerSelection" : "")."'" ?> href="javascript:void(0)" onclick="location.href='index.php?choice=2'">
                <img src="./assets/img/scissors.png" alt="ciseaux"/>
            </button>
        </div>

        <?php if($choice != null){ ?>
        <div>
            <span>VS</span>
        </div>

        <div class="container computer">    
            <?php if($drawComputerText == "Pierre"){ ?>
                <img class='btn' src="./assets/img/rock.png" alt="pierre"/>
            <?php } ?>

            <?php if($drawComputerText == "Ciseaux"){ ?>
                <img class='btn' src="./assets/img/scissors.png" alt="ciseaux"/>
            <?php } ?>

            <?php if($drawComputerText == "Feuille"){ ?>
                <img class='btn' src="./assets/img/paper.png" alt="feuille"/>
            <?php } ?>
        </div>
        <?php } ?>
    </div>           

    <button type="button" class="btn-replay" href="javascript:void(0)" onclick="location.href='index.php'">
        <p>Rejouez</p>
    </button>

    <?php if($stat->getNbParty() > 0){ 
        $nbVictory = $stat->getVictory();
        $nbParty = $stat->getNbParty();
        $nbEgality = $stat->getEgality();
        ?>
    <div class="container-stat" id="stat">
        <span>Vous avez remportés <?php echo strval($nbVictory)." parties sur ".strval($nbParty); ?></span>
        <span><?php echo "L'ordinateur à remporté ".strval($nbParty - $nbVictory - $nbEgality)." parties"; ?></span>
        <span>Heure où a débuté la première partie : <?php echo $_SESSION['startTime']; ?></span>
        
    </div>
    <?php } ?>

    <footer class="footer">
        © Copyright Mr Morel, Mme Roué.
    </footer>
</body>

<script src="assets/js/script.js"></script>

</html>