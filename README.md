# Campus Contest 2021

Campus contest 2021 - realized for Campus Academy Rennes

# Shifumi

Ce projet répond à la problématique posée lors du contest proposé par Mr Duclos.

## Comprendre le projet

Le projet consiste a créer une page internet responsive contenant le jeu Shifumi.
Le site doit être implémenté avec une AI qui doit exécuter des actions.

## Comment installer le projet ?

Le projet utilise une base de données et nécessite donc l'utilisation d'un serveur local (par exemple : Wamp Apache).
Tout d'abord, clonez la totalité du dossier puis ouvrez phpMyAdmin. Pour créer la base de données, importez le fichier approprié.
A partir de votre serveur virtuel, vous pourrez alors lancer le site.

## Comment jouer ?

Il vous suffit de faire des choix à partir des différentes propositions qui vous sont faites. Pour cela, il vous suffit de cliquer sur l'action souhaitée.
Et surtout, amusez-vous !

## Les créateurs

- Mr Morel Lilian (B1 Switch IT)
- Mme Roué Aurélie (B1 Switch IT)
